﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TestNinja.Fundamentals;

namespace TestNinja.UnitTests
{
    [TestFixture]
    public class StackTests
    {
        private Fundamentals.Stack<string> _stack; 
        [SetUp]
        public void SetUp()
        {
            _stack = new Fundamentals.Stack<string>();
        }
        [Test]
        public void Push_NullArgument_ThrowArgumentNullException()
        {
            Assert.That(() => _stack.Push(null), Throws.ArgumentNullException);
        }
        [Test]
        public void Push_ValidArgument_AddToList()
        {
            _stack.Push("Hello");

            Assert.That(_stack.Count, Is.EqualTo(1));
        }
        [Test]
        public void Pop_EmptyList_ThrowInvalidOperationException()
        {
            Assert.That(() => _stack.Pop(), Throws.InvalidOperationException);
        }
        [Test]
        public void Pop_PopulatedList_ReduceCount()
        {
            _stack.Push("Seun");
            var initialCount = _stack.Count;
            var result = _stack.Pop();
            Assert.That(_stack.Count, Is.EqualTo(initialCount - 1));
        }
        [Test]
        public void Pop_PopulatedList_ReturnLastPush()
        {
            string word = "Seun";
            _stack.Push(word);
          
            var result = _stack.Pop();
            Assert.That(result, Is.EqualTo(word));
        }
        [Test]
        public void Peek_EmptyList_ThrowInvalidOperationException()
        {
            Assert.That(() => _stack.Peek(), Throws.InvalidOperationException);
        }
        [Test]
        public void Peek_PopulatedList_ReturnLastPush()
        {
            string word = "Seun";
            _stack.Push(word);

            var result = _stack.Peek();
            Assert.That(result, Is.EqualTo(word));
        }
        [Test]
        public void Peek_PopulatedList_DoNotReduceCount()
        {
            _stack.Push("Seun");
            var initialCount = _stack.Count;
            var result  = _stack.Peek();
            Assert.That(_stack.Count, Is.EqualTo(initialCount));
        }
    }
}
