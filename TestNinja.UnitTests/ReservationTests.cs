﻿using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestNinja.Fundamentals;
using NUnit.Framework;

namespace TestNinja.UnitTests
{
    [TestFixture]
    public class ReservationTests
    {
        [Test]
        public void CanBeCancelledBy_AdminUser_ReturnsTrue()
        {
            //arrange
            var reservation = new Reservation();
            //act
            var result = reservation.CanBeCancelledBy(new User { IsAdmin = true });
            //assert
            Assert.IsTrue(result);
        }

        [Test]
        public void CanBeCancelledBy_Owner_ReturnsTrue()
        {
            //arrange
            var reservation = new Reservation();
            User user = new User();
            
            //act
            reservation.MadeBy = user;
            var result = reservation.CanBeCancelledBy(user);

            //assert
            Assert.IsTrue(result);
        }
        [Test]
        public void CanBeCancelledBy_NotAdminUser_ReturnsFalse()
        {
            //arrange
            var reservation = new Reservation();
            //act
            var result = reservation.CanBeCancelledBy(new User { IsAdmin = false });
            //assert
            Assert.IsFalse(result);
        }
        [Test]
        public void CanBeCancelledBy_NotOwner_ReturnsFalse()
        {
            //arrange
            var reservation = new Reservation();
            User owner = new User();
            User notOwner = new User();
            //act
            reservation.MadeBy = owner;
            var result = reservation.CanBeCancelledBy(notOwner);
            //assert
            Assert.IsFalse(result);
        }
    }
}
