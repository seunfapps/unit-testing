﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestNinja.Fundamentals;

namespace TestNinja.UnitTests
{
    [TestFixture]
    public class FizzBuzzTests
    {
        //[Test]
        //public void GetOutput_DivisibleBy3_ReturnFizz()
        //{
        //    var result = FizzBuzz.GetOutput(9);
        //    Assert.That(result, Is.EqualTo("Fizz"));
        //}
        //[Test]
        //public void GetOutput_DivisibleBy5_ReturnBuzz()
        //{
        //    var result = FizzBuzz.GetOutput(10);
        //    Assert.That(result, Is.EqualTo("Buzz"));
        //}
        //[Test]
        //public void GetOutput_DivisibleBy3And5_ReturnFizzBuzz()
        //{
        //    var result = FizzBuzz.GetOutput(15);
        //    Assert.That(result, Is.EqualTo("FizzBuzz"));
        //}
        //[Test]
        //public void GetOutput_NotDivisibleBy3Or5_ReturnNumber()
        //{
        //    var result = FizzBuzz.GetOutput(7);
        //    Assert.That(result, Is.EqualTo("7"));
        //}

        [Test]
        [TestCase(3, "Fizz")]
        [TestCase(5, "Buzz")]
        [TestCase(15, "FizzBuzz")]
        [TestCase(7, "7")]
        public void GetOutput_WhenCalled_ReturnsString(int input, string expectedOutput)
        {
            var result = FizzBuzz.GetOutput(input);

            Assert.That(result, Is.EqualTo(expectedOutput));
        }
    }
}
