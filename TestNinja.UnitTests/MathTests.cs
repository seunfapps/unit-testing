﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestNinja.Fundamentals;

namespace TestNinja.UnitTests
{
    [TestFixture]
    public class MathTests
    {
        private Fundamentals.Math _math;
        [SetUp]
        public void SetUp()
        {
            //Arrabge
            _math = new Fundamentals.Math();
        }
        [Test]
        //[Ignore("Because I feel like")]
        public void Add_WhenCalled_ReturnsSumOfArguments()
        {
         
            var result = _math.Add(1, 1);

            Assert.That(result, Is.EqualTo(2));
        }

        [Test]
        [TestCase(1,2,2)]
        [TestCase(2,1,2)]
        [TestCase(1,1,1)]
        public void Max_WhenCalled_ReturnsGreaterArgument(int a,int b,int expectedOutput)
        {
            var result = _math.Max(a, b);
            Assert.That(result, Is.EqualTo(expectedOutput));
        }

        [Test]
        public void GetOddNumbers_LimitIsGreaterThanZero_ReturnsAnArrayOfOddNumberUpToLimit()
        {
            var result = _math.GetOddNumbers(5);

            //Assert.That(result, Is.Not.Empty);
            //Assert.That(result.Count(), Is.EqualTo(3));

            //Assert.That(result, Does.Contain(1));
            //Assert.That(result, Does.Contain(2));
            //Assert.That(result, Does.Contain(3));

            //Assert.That(result, Is.Ordered);
            //Assert.That(result, Is.Unique);

            Assert.That(result, Is.EquivalentTo(new[] { 1, 3, 5 }));
        }
    }
}
