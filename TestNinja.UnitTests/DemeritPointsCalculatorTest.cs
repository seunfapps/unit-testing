﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TestNinja.Fundamentals;

namespace TestNinja.UnitTests
{
    [TestFixture]
    public class DemeritPointsCalculatorTest
    {
        private DemeritPointsCalculator _calculator;
        [SetUp]
        public void SetUp()
        {
            _calculator = new DemeritPointsCalculator();
        }
        [Test]
        [TestCase(-1)]
        [TestCase(301)]
        public void CalculateDemeritPoints_SpeedIsLessThanZeroOrGreaterThanMax_ThrowsArgumentOutOfRangeException(int speed)
        {
            Assert.That(() => _calculator.CalculateDemeritPoints(speed), Throws.TypeOf<ArgumentOutOfRangeException>());
        }
        [Test]
        [TestCase(10)]
        [TestCase(65)]
        public void CalculateDemeritPoints_SpeedLessOrEqualsSpeedLimit_ReturnsZero(int speed)
        {
            var result = _calculator.CalculateDemeritPoints(speed);

            Assert.That(result, Is.EqualTo(0));
        }
        [Test]
        public void CalculateDemeritPoints_SpeedGreaterThanSpeedLimit_ReturnDemeritPoints()
        {
            var result = _calculator.CalculateDemeritPoints(70);

            Assert.That(result, Is.EqualTo(1));
        }
    }
}
